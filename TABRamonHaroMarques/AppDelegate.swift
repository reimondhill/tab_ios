//
//  AppDelegate.swift
//  TABRamonHaroMarques
//
//  Created by Ramon Haro Marques on 05/04/2019.
//  Copyright © 2019 ramonharomarques. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    //MARK:- Variables
    //MARK: Constants
    ///returns an instance of the AppDelegate
    static let shared:AppDelegate = UIApplication.shared.delegate as! AppDelegate

    
    //MARK: Vars
    var window: UIWindow?
    
    
    
    //MARK:- Lifecycle methods
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        setupNavigationBarStyle()
        
        let viewControllerToPresent = UINavigationController(rootViewController: ArticlesViewController())
        
        self.window?.rootViewController = viewControllerToPresent
        self.window?.makeKeyAndVisible()
        
        return true
        
    }

    func applicationWillResignActive(_ application: UIApplication) {
       
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
       
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
       
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
       
    }

    func applicationWillTerminate(_ application: UIApplication) {
      
    }

    
    //MARK:- Private methods
    ///Customises the Status and the NavigationBar style
    private func setupNavigationBarStyle(){
        
        UINavigationBar.appearance().barTintColor = UIColor.navigationBarBackground
        UINavigationBar.appearance().tintColor = UIColor.navigationBarTint
        
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.navigationBarText]
        
        UINavigationBar.appearance().isTranslucent = false
        
    }
    
    
    //MARK:- Public methods

}
