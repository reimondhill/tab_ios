//
//  CaseStudy.swift
//  TABRamonHaroMarques
//
//  Created by Ramon Haro Marques on 07/04/2019.
//  Copyright © 2019 ramonharomarques. All rights reserved.
//

import Foundation

struct CaseStudy:Decodable {
    
    let aricles: [Article]
    
    enum CodingKeys: String, CodingKey {
        case aricles = "case_studies"
    }
    
}
