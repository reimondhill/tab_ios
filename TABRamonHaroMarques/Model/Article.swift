//
//  CaseStudy.swift
//  TABRamonHaroMarques
//
//  Created by Ramon Haro Marques on 06/04/2019.
//  Copyright © 2019 ramonharomarques. All rights reserved.
//

import UIKit

struct Article: Decodable {
    
    enum CodingKeys: String, CodingKey {
        
        case client
        case teaser
        case vertical
        case isEnterprise = "is_enterprise"
        case title
        case heroImageURL = "hero_image"
        case sections
        case appStoreURL = "app_store_url"
        
    }
    
    let client: String?
    let title: String?
    let teaser:String?
    let vertical: String?
    let isEnterprise: Bool
    let heroImageURL: URL?
    let appStoreURL: URL?
    
    let sections: [Section]
    
    struct Section: Decodable {
        let title: String?
        let body: [BodyElement]
        
        enum BodyElement: Decodable {
            case text(String)
            case image(URL)
        }
        
        enum CodingKeys: String, CodingKey {
            case title, body = "body_elements"
        }
    }
    
}

//MARK:- Parsing body elements
extension Article.Section.BodyElement {
    
    private enum ImageKeys: String, CodingKey {
        case imageURL = "image_url"
    }
    
    init(from decoder: Decoder) throws {
        if let text = try? decoder.singleValueContainer().decode(String.self) {
            self = .text(text)
        } else if let json = try? decoder.container(keyedBy: ImageKeys.self) {
            let url = try json.decode(URL.self, forKey: .imageURL)
            self = .image(url)
        } else {
            throw Article.Section.BodyElement.decodingError(for: decoder)
        }
    }
    
    private static func decodingError(for decoder: Decoder) -> DecodingError {
        let description = """
        Expected a String or a Dictionary containing a
        '\(ImageKeys.imageURL.stringValue)' key with value.
        """
        return .typeMismatch(self, .init(codingPath: decoder.codingPath, debugDescription: description))
    }
    
}
