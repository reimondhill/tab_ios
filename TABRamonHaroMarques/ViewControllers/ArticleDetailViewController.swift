//
//  ArticleDetailViewController.swift
//  TABRamonHaroMarques
//
//  Created by Ramon Haro Marques on 06/04/2019.
//  Copyright © 2019 ramonharomarques. All rights reserved.
//

import UIKit
import SDWebImage

class ArticleDetailViewController: BaseViewController, UIScrollViewDelegate {

    //MARK:- Variables
    //MARK: Constants
    
    
    //MARK: Vars
    override var isScrolling: Bool{
        get{
            return true
        }
        set{
            super.isScrolling = newValue
        }
    }
    
    var article:Article
    
    
    
    //MARK:- IBOutlets
    @IBOutlet weak var scrollView: UIScrollView!{
        didSet{
            scrollView.bounces = false
            scrollView.delegate = self
        }
    }
    
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var headerView:UIView!
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerImageView: UIImageView!
    @IBOutlet weak var headerBackgroundView: UIView!{
        didSet{
            headerBackgroundView.backgroundColor = UIColor.accentColor.withAlphaComponent(0.45)
        }
    }
    @IBOutlet weak var headerTitleLabel: BaseLabel!
    
    @IBOutlet weak var sectionContainerView: UIView!{
        didSet{
            sectionContainerView.backgroundColor = .white
        }
    }
    
    
    
    //MARK:- IBActions
    
    
    
    //MARK:- Constructor
    init(withCaseStudy article:Article) {
        
        self.article = article
        super.init(nibName: ArticleDetailViewController.identifier, bundle: nil)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        fatalError("Ups.. Not ready for that.")
        
    }

    
    
    //MARK:- Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()

        titleView.title = article.title
        headerTitleLabel.text = article.title
        
        headerImageView.sd_setImage(with: article.heroImageURL) { [weak self] (image, _, _, _) in
            
            self?.updateLogoImageViewSize(withSize: image?.size)
            
        }
        
        populateSectionView()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        updateLogoImageViewSize(withSize: headerImageView.image?.size)
    
    }
    
    
    
    //MARK:- Private methods
    private func populateSectionView(){
        
        print("\(logClassName) populating the section view")
        let sectionStackView = UIStackView()
        
        sectionStackView.translatesAutoresizingMaskIntoConstraints = false
        sectionStackView.axis = .vertical
        sectionStackView.distribution = .fill
        sectionStackView.alignment = .fill
        sectionStackView.spacing = Margins.medium
    
        //Structs pass by copy. Avoinding extra use of memory
        for sectionIndex in article.sections.indices{
            
            let aSectionStackView = UIStackView()
            
            aSectionStackView.axis = .vertical
            aSectionStackView.distribution = .fill
            aSectionStackView.alignment = .fill
            aSectionStackView.spacing = Margins.medium
            
            if let title = article.sections[sectionIndex].title{

                let titleLabel = BaseLabel(withConfiguration: .normalTitle)
                titleLabel.text = title
                aSectionStackView.addArrangedSubview(titleLabel)
                
    
            }
            
            //Structs pass by copy. Avoinding extra use of memory
            for sectionBodyIndex in article.sections[sectionIndex].body.indices{
                
                let aSectionBodyStackView = UIStackView()
                
                aSectionBodyStackView.axis = .vertical
                aSectionBodyStackView.distribution = .fillProportionally
                aSectionBodyStackView.alignment = .fill
                aSectionBodyStackView.spacing = Margins.xLarge
                
                switch article.sections[sectionIndex].body[sectionBodyIndex]{
                case .text(let text):
                    
                    let sectionLabel = BaseLabel(withConfiguration: .normal)
                    
                    sectionLabel.numberOfLines = 0
                    sectionLabel.text = text
                    
                    //To be confirmed but apparently the first section body when title is null is bold
                    if sectionIndex == 0 && sectionBodyIndex == 0 && article.sections[sectionIndex].title == nil{
                        
                        sectionLabel.font = UIFont.systemFont(ofSize: sectionLabel.font.pointSize, weight: .bold)
                        
                    }
                    
                    aSectionBodyStackView.addArrangedSubview(sectionLabel)
                    
                case .image(let imageURL):
                    
                    let sectionImageView = UIImageView()
                    
                    sectionImageView.translatesAutoresizingMaskIntoConstraints = false
                    aSectionBodyStackView.addArrangedSubview(sectionImageView)
                    
                    sectionImageView.contentMode = .scaleAspectFit
                    sectionImageView.leadingAnchor.constraint(equalTo: aSectionBodyStackView.leadingAnchor).isActive = true
                    sectionImageView.trailingAnchor.constraint(equalTo: aSectionBodyStackView.trailingAnchor).isActive = true

                    sectionImageView.sd_setImage(with: imageURL) { (image, _, _, _) in
                        
                        if let image = image{
                            sectionImageView.widthAnchor.constraint(equalTo: aSectionBodyStackView.widthAnchor, multiplier: 1, constant: 0).isActive = true
                            sectionImageView.heightAnchor.constraint(equalTo: aSectionBodyStackView.widthAnchor, multiplier: (1 / image.size.aspectRatio), constant: 0).isActive = true
                            
                        }
                        
                    }
                    
                }
                
                aSectionStackView.addArrangedSubview(aSectionBodyStackView)
                
            }
            
            sectionStackView.addArrangedSubview(aSectionStackView)
            
        }
        
        sectionContainerView.addSubview(sectionStackView)
        sectionStackView.constraintToSuperViewEdges()
        
    }
    
    private func updateLogoImageViewSize(withSize size:CGSize?){
        
        //print("\(logClassName) updating logo image view")
        if let size = size, size.width > 0{
            
            switch AppHelper.traitStatus{
            case .wreghreg, .wcomhreg:
                headerViewHeightConstraint.constant = 2 * (contentView.frame.width * (size.height / size.width))
            case .wreghcom, .wcomhcom:
                headerViewHeightConstraint.constant = (contentView.frame.width * (size.height / size.width))
            }
            
            
        }
        else{
            headerViewHeightConstraint.constant = 1
        }
        
    }
    
    
    
    //MARK:- Public methods
    
    
    
    //MARK:- UIScrollViewDelegate implementation
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let labelAbsolutePoint = headerTitleLabel.convert(headerTitleLabel.bounds.origin, to: view)
        titleView.updateTitleMargin(-labelAbsolutePoint.y)
        //print("\(logClassName) labelAbsoluteFrame \(labelAbsolutePoint)")
        
    }
    
}
