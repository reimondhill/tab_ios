//
//  ArticlesViewController.swift
//  TABRamonHaroMarques
//
//  Created by Ramon Haro Marques on 06/04/2019.
//  Copyright © 2019 ramonharomarques. All rights reserved.
//

import UIKit


class ArticlesViewController: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    
    //MARK:- Variables
    //MARK: Constants
    
    
    //MARK: Vars
    var didFetch = false
    var articleArray:[Article] = []
    
    
    //MARK:- IBOutlets
    @IBOutlet weak var articleCollectionView: UICollectionView!{
        didSet{
            
            articleCollectionView.bounces = false
            articleCollectionView.backgroundColor = .clear
            
            articleCollectionView.register(UINib(nibName: ArticlesCollectionReusableView.identifier, bundle: nil),
                                           forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                                           withReuseIdentifier: ArticlesCollectionReusableView.identifier)
            articleCollectionView.register(UINib(nibName: ArticleCollectionViewCell.identifier,
                                                 bundle: nil),
                                           forCellWithReuseIdentifier: ArticleCollectionViewCell.identifier)
            
            articleCollectionView.isHidden = true
            
            articleCollectionView.dataSource = self
            articleCollectionView.delegate = self
            
        }
    }
    @IBOutlet weak var messageLabel: BaseLabel!{
        didSet{
            messageLabel.text = ""
            messageLabel.isHidden = true
        }
    }
    
    
    
    //MARK:- IBActions
    
    
    
    //MARK:- Constructor
    init() {
        super.init(nibName: ArticlesViewController.identifier, bundle: nil)
        
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        
        if nibNameOrNil == ArticlesViewController.identifier{
            
            super.init(nibName: nibNameOrNil, bundle: nil)
            
        }
        else{
            
            fatalError("Ups.. You are trying to init with the wrong NIB")
            
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        fatalError("Ups.. Not ready for that.")
        
    }
    
    
    
    //MARK:- Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleView.title = NSLocalizedString("messages.articles", comment: "")
   
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if !didFetch{
            
            didFetch = true
            fetchArticles()
            
        }
        
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        articleCollectionView.performBatchUpdates(nil, completion: nil)
        
    }
    
    
    //MARK:- Private methods
    private func fetchArticles(){
        
        articleCollectionView.isHidden = true
        messageLabel.text = ""
        messageLabel.isHidden = true
        
        AppHelper.showLoader(withTitle: NSLocalizedString("messages.fetchingArticles", comment: ""),
                             viewController: self){ (loaderVC) in
                                
                                NetworkHelper.performGetRequest(withURL: NetworkHelper.caseStudiesApiURL) { (result) in
                                    
                                    DispatchQueue.main.async {
                                        
                                        loaderVC.dismiss(animated: true, completion: { [weak self] in
                                            
                                            self?.updateViewAfterFetch(withResult: result)
                                            
                                        })
                                        
                                    }
                                    
                                }
                                
        }
        
    }

    private func updateViewAfterFetch(withResult result:GetRequestResponse){
        
        switch result{
        case .success(let data):
            
            do{
                
                articleArray = try JSONDecoder().decode(CaseStudy.self, from: data).aricles
                
                if articleArray.count == 0{
                    
                    articleCollectionView.isHidden = true
                    messageLabel.text = NSLocalizedString("messages.noMatches", comment: "")
                    messageLabel.isHidden = false
                
                }
                else{
                
                    articleCollectionView.isHidden = false
                    messageLabel.text = ""
                    messageLabel.isHidden = true
                    articleCollectionView.reloadData()
                
                }
                
            }
            catch let jsonError{
                
                print("\(logClassName) Error parsing response: \(jsonError.localizedDescription)")
                articleArray = []
                
                AppHelper.showConfirmationVC(withTitle: NSLocalizedString("messages.error", comment: ""),
                                             message: NSLocalizedString("messages.error.unableGetContent", comment: ""),
                                             sender: self) { [weak self] (result) in
                                                
                                                if result{
                                                    
                                                    self?.fetchArticles()
                                                
                                                }
                                                else{
                                                    
                                                    self?.articleCollectionView.isHidden = true
                                                    self?.messageLabel.text = NSLocalizedString("messages.unableToGetContent", comment: "")
                                                    self?.messageLabel.isHidden = false
                            
                                                }
                }
                
            }
            
        case .failure(let error):
            
            print("\(logClassName) Error fetching clients: \(error.localizedDescription)")
            articleArray = []
            AppHelper.showConfirmationVC(withTitle: NSLocalizedString("messages.error", comment: ""),
                                         message: NSLocalizedString("messages.error.unableGetContent", comment: ""),
                                         sender: self) { [weak self] (result) in
                                            
                                            if result{
                                                
                                                self?.fetchArticles()
                                            
                                            }
                                            else{
                                            
                                                self?.articleCollectionView.isHidden = true
                                                self?.messageLabel.text = NSLocalizedString("messages.unableToGetContent", comment: "")
                                                self?.messageLabel.isHidden = false
                                            
                                            }
                                            
            }
            
        }
        
    }
    
    
    
    //MARK:- Public methods
    
    
    
    //MARK:- UICollectionView methods
    //MARK: UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return articleArray.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let articleCell:ArticleCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: ArticleCollectionViewCell.identifier, for: indexPath) as! ArticleCollectionViewCell
        
        articleCell.logoImageView.sd_setImage(with: articleArray[indexPath.row].heroImageURL, placeholderImage: nil)
        articleCell.teaserLabel.text = articleArray[indexPath.row].teaser
        
        return articleCell
        
    }
    
    
    //MARK: UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var numOfCells:CGFloat = 1
        
        switch AppHelper.traitStatus {
        case .wreghreg:
            numOfCells = 3
        case .wcomhcom, .wreghcom:
            numOfCells = 2
        default:
            break
        }
        
        var width = ( (collectionView.bounds.width / numOfCells) - ( (2 * Margins.large) / numOfCells ) )
        
        if numOfCells > 1{
            //add extra margin
            width -= Margins.medium
        }
        
        let height = ( (width - (2 * Margins.large) ) / 3 ) + Margins.large + 72 + Margins.large
        
        return CGSize(width: width, height: height)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top:Margins.large,
                            left:Margins.large,
                            bottom:Margins.large,
                            right:Margins.large)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return Margins.large
        
    }
    
    
    //MARK: UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let articleHeader:ArticlesCollectionReusableView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                            withReuseIdentifier: ArticlesCollectionReusableView.identifier,
                                                                            for: indexPath) as! ArticlesCollectionReusableView
        
        articleHeader.titleLabel.text = NSLocalizedString("messages.makingTheWorldBetter", comment: "")
        //articleHeader.backgroundView.backgroundColor = UIColor.accentColor.withAlphaComponent(0.6)
        
        return articleHeader
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        switch AppHelper.traitStatus {
        case .wreghreg, .wcomhreg:
            return CGSize(width: collectionView.frame.width, height: view.frame.height/3)
        default:
            return CGSize(width: collectionView.frame.width, height: view.frame.height/2)
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        navigationController?.pushViewController(ArticleDetailViewController(withCaseStudy: articleArray[indexPath.row]), animated: true)
        
    }
    
}
