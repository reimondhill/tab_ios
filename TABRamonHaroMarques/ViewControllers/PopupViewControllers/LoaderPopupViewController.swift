//
//  LoaderPopupViewController.swift
//  CD Ripper iOS
//
//  Created by Ramon Haro Marques on 13/11/2018.
//  Copyright © 2018 Convert AV. All rights reserved.
//

import UIKit
import SDWebImage

class LoaderPopupViewController: UIViewController {
    
    //MARK:- Variables
    //MARK: Constants

    
    //MARK: Vars
    
    
    
    //MARK:- IBOutlets
    @IBOutlet weak var wrapperView: BaseView!{
        didSet{
            wrapperView.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
            wrapperView.backgroundColor = .clear
        }
    }
    
    @IBOutlet weak var gifImageView: UIImageView!{
        didSet{
            
            if let urlGif = Bundle.main.url(forResource: "elipsis_loader", withExtension: "gif"),
                let gifData = try? Data.init(contentsOf: urlGif){
                
                gifImageView.image = UIImage.sd_image(withGIFData: gifData)
            
            }
            
        }
        
    }
    
    
    
    //MARK:- IBActions
    
    
    
    
    //MARK:- Constructor
    init() {
        
        super.init(nibName: LoaderPopupViewController.identifier, bundle: nil)
        self.initialize()
        
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        
        if nibNameOrNil == LoaderPopupViewController.identifier{
            
            super.init(nibName: nibNameOrNil, bundle: nil)
            self.initialize()
            
        }
        else{
            
            fatalError("Ups.. Easy tiger You are trying to init with the wrong NIB")
            
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    
    private func initialize() {
        
        modalTransitionStyle = .crossDissolve
        modalPresentationStyle = .overFullScreen
        
    }
    
    
    
    //MARK:- Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.mainViewPopupBackground
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.25, options: .curveEaseInOut, animations: {
            
            self.wrapperView.transform = .identity
            
        }, completion: nil)
        
    }
    
    
    
    //MARK:- Private methods
    
    
    
    //MARK:- Public methods
    
    

}
