//
//  BasePopupViewController.swift
//  TABRamonHaroMarques
//
//  Created by Ramon Haro Marques on 06/04/2019.
//  Copyright © 2019 ramonharomarques. All rights reserved.
//

import UIKit

class BasePopupViewController: UIViewController {

    //MARK:- Variables
    //MARK: Constants
    private let defaultTitle = NSLocalizedString("messages.title", comment: "")
    private let statusFrame = UIApplication.shared.statusBarFrame.height
    
    enum ButtonDisplay{
        case both
        case cancelOnly
        case confirmOnly
        case none
    }
    
    
    //MARK: Vars
    private var didLoad = false
    internal var canRotate = true
    override var title: String?{
        didSet{
            if didLoad{
                titleLabel.text = title
            }
        }
    }
    
    internal var isConfirmButtonEnabled:Bool = true{
        didSet{
            confirmButton.isEnabled = isConfirmButtonEnabled
        }
    }
    private final var buttonDisplay:ButtonDisplay = ButtonDisplay.both
    
    internal final var wrapperViewHeight:CGFloat{
        get{
            return wrapperViewHeightConstraint.constant
        }
        set{
            wrapperViewHeightConstraint.constant = newValue
        }
    }
    internal final var headerViewHeight:CGFloat{
        get{
            return headerViewHeightConstraint.constant
        }
        set{
            headerViewHeightConstraint.constant = newValue
        }
    }
    internal final var footerViewHeight:CGFloat{
        get{
            return footerViewHeightConstraint.constant
        }
        set{
            footerViewHeightConstraint.constant = newValue
        }
    }
    
    internal var headerFooterViewHeight:CGFloat{ return headerViewHeight + footerViewHeight }
    
    
    
    //MARK:- IBOutlets
    @IBOutlet weak var scrollView: UIScrollView!{
        didSet{
            scrollView.backgroundColor = .clear
        }
    }
    
    @IBOutlet weak var contentView: UIView!{
        didSet{
            contentView.backgroundColor = UIColor.clear
        }
    }
    
    @IBOutlet weak var wrapperView: BaseView!
    @IBOutlet weak private var wrapperViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var headerView: UIView!{
        didSet{
            headerView.backgroundColor = UIColor.viewPopupHeader
            headerView.addBottomSeparator(color: UIColor.defaultSeparator, height: 1, margins: 0)
        }
    }
    @IBOutlet weak private var headerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleLabel: BaseLabel!{
        didSet{
            titleLabel.text = title == nil ? defaultTitle:title!
        }
    }

    @IBOutlet weak var innerView: UIView!{
        didSet{
            innerView.backgroundColor = UIColor.viewInnerPopupBackground
        }
    }
    
    @IBOutlet weak var footerView: UIView!{
        didSet{
            footerView.backgroundColor = UIColor.viewPopupHeader
            footerView.addTopSeparator(color: UIColor.defaultSeparator, height: 1, margins: 0)
        }
    }
    @IBOutlet weak private var footerViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var buttonsStackView: UIStackView!
    @IBOutlet weak var cancelButton: BaseButton!{
        didSet{
            cancelButton.setTitle(NSLocalizedString("messages.cancel", comment: ""), for: .normal)
        }
    }
    @IBOutlet weak var confirmButton: BaseButton!{
        didSet{
            confirmButton.setTitle(NSLocalizedString("messages.ok", comment: ""), for: .normal)
        }
    }
    
    
    
    //MARK:- IBActions
    @IBAction func confirmAction(_ sender: BaseButton) {
        confirmAction()
    }
    
    @IBAction func cancelAction(_ sender: BaseButton) {
        cancelAction()
    }
    
    
    
    //MARK:- Constructor
    init(buttonDisplay:ButtonDisplay) {
        super.init(nibName: BasePopupViewController.identifier, bundle: nil)
        
        self.buttonDisplay = buttonDisplay
        self.initialize()
        
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        
        if nibNameOrNil == BasePopupViewController.identifier{
            
            super.init(nibName: nibNameOrNil, bundle: nil)
            self.initialize()
            
        }
        else{
            
            fatalError("Ups.. Easy tiger You are trying to init with the wrong NIB")
        
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.initialize()
        
    }
    
    private func initialize() {
        
        modalTransitionStyle = .crossDissolve
        modalPresentationStyle = .overFullScreen
        
    }
    
    
    
    //MARK:- Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.mainViewPopupBackground
        wrapperViewHeight = AppHelper.heightOfSafeArea
        
        switch buttonDisplay {
        case .both:
            break
        case .cancelOnly:
            buttonsStackView.removeArrangedSubview(confirmButton)
            confirmButton.removeFromSuperview()
        case .confirmOnly:
            buttonsStackView.removeArrangedSubview(cancelButton)
            cancelButton.removeFromSuperview()
        case .none:
            buttonsStackView.removeArrangedSubview(confirmButton)
            confirmButton.removeFromSuperview()
            buttonsStackView.removeArrangedSubview(cancelButton)
            cancelButton.removeFromSuperview()
        }
        
        didLoad = true
        titleLabel.text = title
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        wrapperView.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.25, options: .curveEaseInOut, animations: {
            
            self.wrapperView.transform = .identity
            
        }, completion: nil)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
        
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        if canRotate{
         
            coordinator.animateAlongsideTransition(in: contentView, animation: nil) { (_) in
                
                print("\(self.logClassName) \(AppHelper.traitStatus)")
                self.wrapperViewHeight = AppHelper.heightOfSafeArea
                
            }
            
        }
        
    }
    
    
    
    //MARK:- Private methods
    
    
    
    //MARK:- Internal methods
    ///Calculates a Height considering a container. If rquested is bigger then it will return containerHeight.
    internal func calculateHeight(containerSize:CGFloat, desiredHeight:CGFloat)->CGFloat{
        
        if containerSize > desiredHeight{
            print("\(logClassName) I can give you that")
            return desiredHeight
        }
        else{
            print("\(logClassName) UPS, I can't give you that")
            return containerSize
        }
        
    }
    
    internal func confirmAction(){
         //Override this method
    }
    
    internal func cancelAction(){
        dismiss(animated: true, completion: nil)
        
        //Override this method BUT: Think about calling super!
    }
    
    
    
    //MARK:- Public methods


}
