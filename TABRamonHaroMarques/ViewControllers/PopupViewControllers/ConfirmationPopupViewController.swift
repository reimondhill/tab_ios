//
//  ConfirmationPopupViewController.swift
//  TABRamonHaroMarques
//
//  Created by Ramon Haro Marques on 06/04/2019.
//  Copyright © 2019 ramonharomarques. All rights reserved.
//


import UIKit


class ConfirmationPopupViewController: BasePopupViewController {

    //MARK:- Variables
    //MARK: Constants
    
    
    //MARK: Vars
    var message:String?
    var completion:((Bool)->())!
    
    lazy private var messageLabel:BaseLabel = {
        
        let rtLabel = BaseLabel(withConfiguration: .popup)
        
        rtLabel.translatesAutoresizingMaskIntoConstraints = false
        rtLabel.numberOfLines = 0
        rtLabel.textAlignment = .justified
        
        return rtLabel
        
    }()
    
    
    
    //MARK:- Constructor
    init (withMessage message:String, andTitle title:String){
        
        self.message = message
        super.init(buttonDisplay: .both)
        self.title = title
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("Ups.. Not ready for that.")
        
    }
    
    
    
    //MARK:- Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
    
        canRotate = false
        innerView.addSubview(messageLabel)
    
        messageLabel.rightAnchor.constraint(equalTo: innerView.rightAnchor, constant:-Margins.large).isActive = true
        messageLabel.leadingAnchor.constraint(equalTo: innerView.leadingAnchor, constant:Margins.large).isActive = true
        messageLabel.centerYAnchor.constraint(equalTo: innerView.centerYAnchor).isActive = true
       
        messageLabel.text = message
    
        wrapperViewHeight = calculateHeight(containerSize: AppHelper.heightOfSafeArea, desiredHeight: self.calculateDesiredContent())
        
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        coordinator.animate(alongsideTransition: nil) { [weak self](_) in
            
            let newHeight = self!.calculateHeight(containerSize: AppHelper.heightOfSafeArea, desiredHeight: self!.calculateDesiredContent())
            self!.wrapperViewHeight = newHeight
        
        }
        
    }
    
    
    
    //MARK:- Private methods
    private func calculateDesiredContent()->CGFloat{
        
        var contentHeight:CGFloat = headerFooterViewHeight
        let messageLabelHeight:CGFloat = ((message ?? "").heightWithConstrainedWidth(width: (wrapperView.frame.width - (2 * Margins.large)), font: messageLabel.font))
        
        contentHeight += messageLabelHeight
        contentHeight += 4 * Margins.xLarge
    
        return contentHeight
    }
    
    
    override internal func confirmAction() {
        
        dismiss(animated: true) {
            self.completion(true)
        }
        
    }
    
    override internal func cancelAction() {
        
        dismiss(animated: true) {
            self.completion(false)
        }
        
    }
    
    
    
    //MARK:- Public methods

}
