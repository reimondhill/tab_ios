//
//  BaseViewController.swift
//  TABRamonHaroMarques
//
//  Created by Ramon Haro Marques on 06/04/2019.
//  Copyright © 2019 ramonharomarques. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    //TO BE EXTENDED
    //MARK:- Variables
    //MARK: Constants
    
    
    //MARK: Vars
    //Change 5 -> Keep reusing
    internal var isScrolling = false
    lazy internal var titleView:ScrollingNavigationBarTitleView = {
        return ScrollingNavigationBarTitleView(isScrolling: isScrolling)
    }()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    //MARK:- IBOutlets
    
    
    
    //MARK:- IBActions

    
    
    //MARK:- Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.mainViewBackground
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if navigationItem.title == nil{
            navigationItem.titleView = titleView
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        //Change 2 -> Fix some identified bugs
        if let navigationBar = navigationController?.navigationBar{
            
            titleView.bounds = navigationBar.bounds
            
        }
    }
    
    
    //MARK:- Private methods
    
    

    
}


