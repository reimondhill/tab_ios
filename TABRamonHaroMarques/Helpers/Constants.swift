//
//  Constants.swift
//  TABRamonHaroMarques
//
//  Created by Ramon Haro Marques on 06/04/2019.
//  Copyright © 2019 ramonharomarques. All rights reserved.
//

import UIKit


///Custom Error structure
struct AppError:Error{
    
    private let message:String
    var localizedDescription: String { return message }
    
    init(_ message:String) {
        self.message = message
    }
    
}

enum OrientationStatus {
    case landscape
    case portrait
}

//Change 1 -> Improve or simplify
enum DeviceTraitStatus {
    case wreghreg
    case wcomhreg
    case wreghcom
    case wcomhcom
    
    static var current:DeviceTraitStatus{
        
        switch (UIScreen.main.traitCollection.horizontalSizeClass, UIScreen.main.traitCollection.verticalSizeClass){
            
        case (UIUserInterfaceSizeClass.regular, UIUserInterfaceSizeClass.regular):
            return .wreghreg
        case (UIUserInterfaceSizeClass.compact, UIUserInterfaceSizeClass.regular):
            return .wcomhreg
        case (UIUserInterfaceSizeClass.regular, UIUserInterfaceSizeClass.compact):
            return .wreghcom
        case (UIUserInterfaceSizeClass.compact, UIUserInterfaceSizeClass.compact):
            return .wcomhcom
        default:
            return .wcomhreg
            
        }

    }
    
}

enum Direction {
    case up
    case rigth
    case down
    case left
}


struct Constants{
    
    
    
}
