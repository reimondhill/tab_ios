//
//  CGSizeExtension.swift
//  TABRamonHaroMarques
//
//  Created by Ramon Haro Marques on 07/04/2019.
//  Copyright © 2019 ramonharomarques. All rights reserved.
//

import UIKit

extension CGSize{
    
    //Returns the relation between width and height
    var aspectRatio:CGFloat{
        
        if height == 0{
            return 0
        }
        
        return width/height
    
    }
    
}
