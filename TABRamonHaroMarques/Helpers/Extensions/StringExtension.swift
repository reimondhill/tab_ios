//
//  StringExtension.swift
//  TABRamonHaroMarques
//
//  Created by Ramon Haro Marques on 07/04/2019.
//  Copyright © 2019 ramonharomarques. All rights reserved.
//

import UIKit

extension String{
    
    /// Returns the String height for a UILabel by giving the width and the font
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
        
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSAttributedString.Key.font: font], context: nil)
        return boundingBox.height
        
    }
    
}
