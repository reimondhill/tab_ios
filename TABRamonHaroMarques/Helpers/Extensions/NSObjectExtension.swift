//
//  NSObjectExtension.swift
//  TABRamonHaroMarques
//
//  Created by Ramon Haro Marques on 06/04/2019.
//  Copyright © 2019 ramonharomarques. All rights reserved.
//

import Foundation

extension NSObject{
    
    class var logClassName: String{
        return NSStringFromClass(self).components(separatedBy: ".").last!.appending(":")
    }
    
    var logClassName: String{
        return NSStringFromClass(type(of: self)).components(separatedBy:".").last!.appending(":")
    }
    
    class var identifier: String{
        return NSStringFromClass(self).components(separatedBy: ".").last!
    }
    
    var identifier: String{
        return NSStringFromClass(type(of: self)).components(separatedBy:".").last!
    }
    
}
