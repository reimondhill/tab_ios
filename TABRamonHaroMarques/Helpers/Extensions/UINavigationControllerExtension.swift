//
//  UINavigationControllerExtension.swift
//  TABRamonHaroMarques
//
//  Created by Ramon Haro Marques on 07/04/2019.
//  Copyright © 2019 ramonharomarques. All rights reserved.
//

import UIKit

extension UINavigationController {
    
    override open var preferredStatusBarStyle : UIStatusBarStyle {
        return topViewController?.preferredStatusBarStyle ?? .default
    }
    
}
