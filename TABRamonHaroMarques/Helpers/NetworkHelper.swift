//
//  NetworkHelper.swift
//  TABRamonHaroMarques
//
//  Created by Ramon Haro Marques on 06/04/2019.
//  Copyright © 2019 ramonharomarques. All rights reserved.
//

import UIKit

typealias GetRequestResponse = Result<Data,Error>
typealias GetRequestCompletion = (GetRequestResponse)->()

class NetworkHelper: NSObject {
    
    //MARK:- Variables
    //MARK: Constants
    static var caseStudiesApiURLString:String{
        
        #if DEV
        return "https://raw.githubusercontent.com/theappbusiness/ios-challenge/master/endpoints/v2/caseStudies.json"
        #else
        return "https://raw.githubusercontent.com/theappbusiness/ios-challenge/master/endpoints/v1/caseStudies.json"
        #endif

    }
    static let caseStudiesApiURL:URL = URL(string: caseStudiesApiURLString)!
    
    
    
    //MARK: Vars
    
    
    
    
    //MARK:- Public methods
    ///Perform an async get request with a given URL
    static func performGetRequest(withURL url:URL, completion: @escaping GetRequestCompletion){
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            //Handling errors
            if let error = error{
                completion(.failure(error))
                return
            }
            
            //Returning data if available
            if let data = data{
                completion(.success(data))
                return
            }
            
            //Handling some other errors
            if let response = response{

                //Non succes status codes are not handled by an error not nil
                if let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode != 200{

                    completion(.failure(AppError("Destination unreachable -> status code = \(httpURLResponse.statusCode)")))
                    return
                    
                }

            }
            else{

                //Really unlikely to happen
                completion(.failure(AppError("Invalid response")))
                return

            }

            //Unknown handled scenarios
            completion(.failure(AppError("Unkown error")))
            return
            
        }.resume()
        
    }
    
    //Change 7 -> Keep growing up
    static func performRequest(_ urlRequest:URLRequest, completion: @escaping GetRequestCompletion){
        
        URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            
            //Handling errors
            if let error = error{
                completion(.failure(error))
                return
            }
            
            //Returning data if available
            if let data = data{
                completion(.success(data))
                return
            }
            
            //Handling some other errors
            if let response = response{
                
                //Non succes status codes are not handled by an error not nil
                if let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode != 200{
                    
                    completion(.failure(AppError("Destination unreachable -> status code = \(httpURLResponse.statusCode)")))
                    return
                    
                }
                
            }
            else{
                
                //Really unlikely to happen
                completion(.failure(AppError("Invalid response")))
                return
                
            }
            
            //Unknown handled scenarios
            completion(.failure(AppError("Unkown error")))
            return
            
            }.resume()
        
    }
    
}
