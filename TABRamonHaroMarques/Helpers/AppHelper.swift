//
//  AppHelper.swift
//  TABRamonHaroMarques
//
//  Created by Ramon Haro Marques on 06/04/2019.
//  Copyright © 2019 ramonharomarques. All rights reserved.
//

import UIKit

class AppHelper: NSObject {

    //MARK:- Variable
    //Change 1.1 -> Announce
    @available(*, deprecated, renamed: "DeviceTraitStatus.current")
    static var traitStatus:DeviceTraitStatus{
        
        let sizeTraitsClass:(UIUserInterfaceSizeClass, UIUserInterfaceSizeClass) = (UIScreen.main.traitCollection.horizontalSizeClass, UIScreen.main.traitCollection.verticalSizeClass)
        
        switch sizeTraitsClass {
        case (UIUserInterfaceSizeClass.regular, UIUserInterfaceSizeClass.regular):
            //IPAD: Width: Regular, Height: Regular
            return .wreghreg
        case (UIUserInterfaceSizeClass.compact, UIUserInterfaceSizeClass.regular):
            //Any IPHONE Portrait Width: Compact, Height: Regular
            return .wcomhreg
        case (UIUserInterfaceSizeClass.regular, UIUserInterfaceSizeClass.compact):
            //IPHONE Plus Landscape Width: Regular, Height: Compact
            return .wreghcom
        case (UIUserInterfaceSizeClass.compact, UIUserInterfaceSizeClass.compact):
            //IPHONE landscape Width: Compact, Height: Compact
            return .wcomhcom
        default:
            return .wcomhreg
        }
        
    }
    
    //Change 1.1 Same
    static var orientationStatus:OrientationStatus{
        
        switch UIApplication.shared.statusBarOrientation{
        case .portrait:
            return .portrait
        case .portraitUpsideDown:
            return .portrait
        case .landscapeLeft:
            return .landscape
        case .landscapeRight:
            return .landscape
        default:
            return .portrait
        }
        
    }
    
    /// Returns the Height for the safe area
    static var heightOfSafeArea:CGFloat {
        
        guard let rootView = UIApplication.shared.keyWindow else { return 0 }
        
        if #available(iOS 11.0, *) {
            
            let topInset = rootView.safeAreaInsets.top
            let bottomInset = rootView.safeAreaInsets.bottom
            
            return rootView.bounds.height - topInset - bottomInset
            
        } else {
            
            return rootView.bounds.height
            
        }
        
    }
    
    
    
    //MARK:- Public methods
    static func showAlert(withTitle title:String, message:String, sender:UIViewController, completion:(() -> ())?){
        
        let alert = UIAlertController(title: title,
                                      message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("messages.ok", comment: ""), style: .default, handler: { (action) in
            completion?()
        }))
        
        sender.present(alert, animated: true, completion: nil)
        
    }
    
    //Change 3 -> Document
    /// Creates and presents new ConfirmationPopupViewController
    /// - Parameters:
    ///   - title: Title to be displayed
    ///   - message: Message to be displayed
    ///   - sender: The UIViewController that displays will present the ConfirmationPopupViewController
    ///   - completion: Callback when the ConfirmationPopupViewController has been dismissed
    static func showConfirmationVC(withTitle title:String, message:String, sender:UIViewController, completion:@escaping (Bool) -> ()){
        
        let confirmationPVC = ConfirmationPopupViewController(withMessage: message, andTitle: title)
        
        confirmationPVC.completion = completion
        
        sender.present(confirmationPVC, animated: true, completion: nil)
        
    }
    
    static func showLoader(withTitle title:String, viewController:UIViewController, completion:((_ loaderVC:LoaderPopupViewController)->())?){
        
        let loaderPopupViewController = LoaderPopupViewController()
        
        viewController.present(loaderPopupViewController, animated: true) {
            
            completion?(loaderPopupViewController)
            
        }
        
    }
    
}
