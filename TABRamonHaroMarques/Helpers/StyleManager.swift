//
//  StyleManager.swift
//  TABRamonHaroMarques
//
//  Created by Ramon Haro Marques on 05/04/2019.
//  Copyright © 2019 ramonharomarques. All rights reserved.
//

import UIKit



extension UIColor{
    
    //MARK:- App Colors Extension
    //They are computed variables and not constants because in the future they can be extended (Different targets).
    //MARK: Not UI
    static var accentColor:UIColor {
        return UIColor(red: 250.0/255.0, green: 77.0/255.0, blue: 25.0/255.0, alpha: 1)
    }
    
    static var defaultSeparator:UIColor {
        return UIColor.clear
    }
    
    static var defaultTint:UIColor{
        return mainTextColor
    }

    
    //MARK: Status, Toolbar and NavigationBar
    static var statusBarBackground:UIColor{
        return accentColor
    }
    
    static var navigationBarBackground:UIColor{
        return accentColor
    }
    static var navigationBarTint:UIColor{
        return UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1)
    }
    static var navigationBarText:UIColor{
        return UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1)
    }
    static var toolbar:UIColor{
        return accentColor
    }
    
    
    //MARK: UIView
    static var mainViewBackground:UIColor{
        return UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1)
    }

    static var secondaryViewBackground:UIColor{
        return UIColor(red: 229.0/255.0, green: 229.0/255.0, blue: 229.0/255.0, alpha: 1)
    }
    
    static var mainBorder:UIColor{
        return UIColor.clear
    }
    
    static var secondaryBorder:UIColor{
        return UIColor.clear
    }
    
    static var cellViewBackground:UIColor{
        return secondaryViewBackground
    }
    
    static var cellViewSelectedBackground:UIColor{
        return accentColor
    }
    
    static var mainButtonBackground:UIColor{
        return accentColor
    }
    
    static var mainButtonBorder:UIColor{
        return mainButtonBackground
    }
    
    static var mainButtonDisabledBackground:UIColor{
        return .lightGray
    }
    
    static var mainButtonDissabledBorder:UIColor{
        return mainButtonDisabledBackground
    }
    
    static var secondaryButtonBackground:UIColor{
        return accentColor
    }
    
    static var secondaryButtonBorder:UIColor{
        return mainButtonBackground
    }
    
    static var secondaryButtonDisabledBackground:UIColor{
        return .lightGray
    }
    
    static var secondaryButtonDissabledBorder:UIColor{
        return mainButtonDisabledBackground
    }
    
    
    //Popup
    static var mainViewPopupBackground:UIColor{
        return UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.2)
    }
    
    static var viewPopupHeader:UIColor{
        return secondaryViewBackground//UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1)
    }
    
    static var viewInnerPopupBackground:UIColor{
        return mainViewBackground
    }
    
    static var mainPopupButtonBackground:UIColor{
        return accentColor
    }
    
    static var mainPopupButtonBorder:UIColor{
        return mainButtonBackground
    }
    
    static var mainPopupButtonDisabledBackground:UIColor{
        return UIColor(red: 235/255.0, green: 235/255.0, blue: 235/255.0, alpha: 1)
    }
    
    static var mainPopupButtonDissabledBorder:UIColor{
        return mainButtonDisabledBackground
    }
    
    static var secondaryPopupButtonBackground:UIColor{
        return secondaryViewBackground
    }
    
    static var secondaryPopupButtonBorder:UIColor{
        return secondaryPopupButtonBackground
    }
    
    static var secondaryPopupButtonDisabledBackground:UIColor{
        return secondaryPopupButtonBackground
    }
    
    static var secondaryPopupButtonDissabledBorder:UIColor{
        return mainButtonDisabledBackground
    }
    
    
    //MARK: Text color
    static var mainTextColor:UIColor{
        return UIColor(red: 74.0/255.0, green: 74.0/255.0, blue: 74.0/255.0, alpha: 1)
    }
    
    static var mainHighlightedTextColor:UIColor{
        return UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1)
    }
    
    static var secondaryTextColor:UIColor{
        //Not applicable yet
        return UIColor(red: 74.0/255.0, green: 74.0/255.0, blue: 74.0/255.0, alpha: 1)
    }
    
    static var popupTitleTextColor:UIColor{
        return mainTextColor
    }
    
    static var mainButtonTextColor:UIColor{
        return mainHighlightedTextColor
    }
    
    static var mainButtonDisabledTextColor:UIColor{
        return mainTextColor
    }
    
    static var secondaryButtonTextColor:UIColor{
        return mainTextColor
    }
    
    static var secondaryButtonDisabledTextColor:UIColor{
        return mainTextColor
    }
    
    static var mainPopupButtonTextColor:UIColor{
        return mainHighlightedTextColor
    }
    
    static var mainPopupButtonDisabledTextColor:UIColor{
        return mainTextColor
    }

    static var secondaryPopupButtonTextColor:UIColor{
        return mainTextColor
    }
    
    static var secondaryPopupButtonDisabledTextColor:UIColor{
        return mainTextColor
    }
    
    //TODO more might be coming
    
}

struct Margins {

    static let xSmall:CGFloat = 3
    static let small:CGFloat = 4
    static let medium:CGFloat = 8
    static let large:CGFloat = 12
    static let xLarge:CGFloat = 18
    static let xxLarge:CGFloat = 24
    
}

struct TextSize {
    
    static let navigationTitle:CGFloat = 17
    //Change 1.3 -> Apply
    static let normalSmall:CGFloat = DeviceTraitStatus.current == .wreghreg ? 16:13
    static let normal:CGFloat = AppHelper.traitStatus == .wreghreg ? 18:15
    
    static let titleBig:CGFloat = AppHelper.traitStatus == .wreghreg ? 32:26
    static let title:CGFloat = AppHelper.traitStatus == .wreghreg ? 20:18
    static let subTitle:CGFloat = AppHelper.traitStatus == .wreghreg ? 18:16
    
}

struct BorderRadius {
    
    static let xSmall:CGFloat = 3
    static let small:CGFloat = 5
    static let medium:CGFloat = 10
    static let large:CGFloat = 15
    static let xlarge:CGFloat = 20
    static let xxlarge:CGFloat = 25

}

struct Separators {
    
    static let xSmall:CGFloat = 1
    static let small:CGFloat = 3
    static let medium:CGFloat = 6
    static let large:CGFloat = 8
    static let xlarge:CGFloat = 12
    static let xxlarge:CGFloat = 15
    
}

