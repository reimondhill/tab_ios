//
//  ArticleCollectionViewCell.swift
//  TABRamonHaroMarques
//
//  Created by Ramon Haro Marques on 06/04/2019.
//  Copyright © 2019 ramonharomarques. All rights reserved.
//

import UIKit

class ArticleCollectionViewCell: UICollectionViewCell {
    
    //MARK:- Variables
    //MARK: Constants
    
    
    //MARK: Vars
    
    
    
    //MARK:- IBOutlets
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var teaserLabel: BaseLabel!{
        didSet{
            teaserLabel.highlightedTextColor = UIColor.mainHighlightedTextColor
        }
    }
    
    
    //MARK:- IBActions
    

    
    //MARK:- Lifecycle methods
    override func awakeFromNib() {
        super.awakeFromNib()
        
        addBorder(borderWidth: Separators.xSmall,
                  radius: BorderRadius.medium,
                  color: UIColor.defaultSeparator)
        
        backgroundColor = UIColor.cellViewBackground
        
        let selectedBackgroundView = UIView()
        selectedBackgroundView.backgroundColor = UIColor.cellViewSelectedBackground
        self.selectedBackgroundView = selectedBackgroundView
        
    }
    
    
    
    //MARK:- Private methods
    
    
    
    //MARK:- Public methods


}
