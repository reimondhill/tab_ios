//
//  ArticlesCollectionReusableView.swift
//  TABRamonHaroMarques
//
//  Created by Ramon Haro Marques on 07/04/2019.
//  Copyright © 2019 ramonharomarques. All rights reserved.
//

import UIKit

class ArticlesCollectionReusableView: UICollectionReusableView {

    //MARK:- Variables
    //MARK: Constants
    
    
    //MARK: Vars
    
    
    
    //MARK:- IBOutlets
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var titleLabel: BaseLabel!
    
    
    //MARK:- IBActions
    
    
    
    //MARK:- Constructor
    
    
    
    //MARK:- Lifecycle methods
    
    
    
    //MARK:- Private methods
    
    
    
    //MARK:- Public methods

}
