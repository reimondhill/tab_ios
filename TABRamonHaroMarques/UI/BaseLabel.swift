//
//  BaseLabel.swift
//  TABRamonHaroMarques
//
//  Created by Ramon Haro Marques on 07/04/2019.
//  Copyright © 2019 ramonharomarques. All rights reserved.
//

import UIKit

class BaseLabel: UILabel {

    //MARK:- Variables
    //MARK: Constants
    enum LabelConfiguration:Int, CaseIterable{
        case normal = 0
        case normalSmall = 1
        case normalBigTitle = 2
        case normalTitle = 3
        case normalSubtitle = 4
        
        case normalHigligthed = 5
        case normalSmallHigligthed = 6
        case normalBigTitleHigligthed = 7
        case normalTitleHigligthed = 8
        case normalSubtitleHigligthed = 9
        
        case popup = 10
        case popTitle = 11
        
        case navBar = 12
        //More future cases
    }
    
    
    //MARK: Vars
    var labelConfiguration:LabelConfiguration = .normal{
        didSet{
            setupView()
        }
    }
    @IBInspectable var labelConfigurationOption:Int{
        get{
            return labelConfiguration.rawValue
        }
        set{
            guard let labelConfigurationTemp = LabelConfiguration(rawValue: newValue) else{ return }
            labelConfiguration = labelConfigurationTemp
        }
    }
    
    
    
    //MARK:- Constructor
    init(withConfiguration labelConfiguration:LabelConfiguration) {
        
        self.labelConfiguration = labelConfiguration
        super.init(frame: CGRect())
        
        backgroundColor = .clear
        //DidSet never gets called on the init method
        setupView()
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupView()
        
    }
    
    
    //MARK:- Lifecycle methods
    override func layoutSubviews() {
        super.layoutSubviews()
        
        //Future if handling different label configurations for portrait and landscape
        
    }
    
    
    //MARK:- Private methods
    private func setupView(){
        
        switch labelConfiguration {
            
        case .normal, .normalHigligthed:
            textColor = labelConfiguration == .normal ? UIColor.mainTextColor:UIColor.mainHighlightedTextColor
            font = UIFont.systemFont(ofSize: TextSize.normal, weight: .regular)
        
        case .popup:
            textColor = UIColor.secondaryTextColor
            font = UIFont.systemFont(ofSize: TextSize.normal, weight: .regular)
            
        case .normalSmall, .normalSmallHigligthed:
            textColor = labelConfiguration == .normalSmall ? UIColor.mainTextColor:UIColor.mainHighlightedTextColor
            font = UIFont.systemFont(ofSize: TextSize.normalSmall, weight: .light)
        
        case .normalBigTitle, .normalBigTitleHigligthed:
            textColor = labelConfiguration == .normalTitle ? UIColor.mainTextColor:UIColor.mainHighlightedTextColor
            font = UIFont.systemFont(ofSize: TextSize.titleBig, weight: .bold)
            
        case .normalTitle, .normalTitleHigligthed:
            textColor = labelConfiguration == .normalTitle ? UIColor.mainTextColor:UIColor.mainHighlightedTextColor
            font = UIFont.systemFont(ofSize: TextSize.title, weight: .bold)
        
        case .popTitle:
            textColor = UIColor.popupTitleTextColor
            font = UIFont.systemFont(ofSize: TextSize.navigationTitle, weight: .bold)
            
        case .normalSubtitle, .normalSubtitleHigligthed:
            textColor = labelConfiguration == .normalSubtitle ? UIColor.mainTextColor:UIColor.mainHighlightedTextColor
            font = UIFont.systemFont(ofSize: TextSize.subTitle, weight: .semibold)
            
        case .navBar:
            textColor = UIColor.mainHighlightedTextColor
            font = UIFont.systemFont(ofSize: TextSize.navigationTitle, weight: .semibold)
            
        }
        
    }
    
    
    //MARK:- Public methods

}
