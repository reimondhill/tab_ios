//
//  MainButton.swift
//  Convert AV
//
//  Created by Ramon Haro Marques on 25/10/2018.
//  Copyright © 2018 Convert AV. All rights reserved.
//

import UIKit

class BaseButton: UIButton {

    //MARK:- Variables
    //MARK: Constants
    enum ButtonConfiguration:Int{
        case normal = 0
        case normalSecondary = 1
        case popup = 2
        case popupSecondary = 3
    }
    
    
    
    //MARK: Vars
    @IBInspectable var cornerRadius:CGFloat = BorderRadius.medium{
        didSet{
            layer.cornerRadius = cornerRadius
            setNeedsDisplay()
        }
    }
    @IBInspectable var borderWidth:CGFloat = Separators.xSmall{
        didSet{
            layer.borderWidth = borderWidth
            setNeedsDisplay()
        }
    }

    var buttonConfiguration:ButtonConfiguration = .normal{
        didSet{
            setupView()
        }
    }
    @IBInspectable var buttonConfigurationOption:Int{
        get{
            return buttonConfiguration.rawValue
        }
        set{
            guard let buttonConfigurationTemp = ButtonConfiguration(rawValue: newValue) else{ return }
            buttonConfiguration = buttonConfigurationTemp
        }
    }
    

    override var isEnabled: Bool{
        didSet{
        
            switch buttonConfiguration {
            case .normal:
                layer.borderColor = isEnabled ? UIColor.mainButtonBorder.cgColor : UIColor.mainButtonDissabledBorder.cgColor
            case .normalSecondary:
                layer.borderColor = isEnabled ? UIColor.secondaryButtonBorder.cgColor : UIColor.secondaryButtonDisabledBackground.cgColor
            case .popup:
                layer.borderColor = isEnabled ? UIColor.mainPopupButtonBorder.cgColor : UIColor.mainPopupButtonDissabledBorder.cgColor
            case .popupSecondary:
                layer.borderColor = isEnabled ? UIColor.secondaryPopupButtonBorder.cgColor : UIColor.secondaryPopupButtonDissabledBorder.cgColor
            }
            
            setNeedsDisplay()
            
        }
    }
    
    
    
    //MARK:- Constructor
    init(withConfiguration buttonConfiguration:ButtonConfiguration) {
        
        self.buttonConfiguration = buttonConfiguration
        super.init(frame: CGRect())
        
        //DidSet never gets called on the init method
        setupView()
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    
        setupView()
    
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupView()
        
    }
    
    
    
    //MARK:- Private methods
    private func setupView(){
        
        layer.borderWidth = borderWidth
        layer.cornerRadius = cornerRadius
        
        switch buttonConfiguration {
            
        case .normal:
            
            backgroundColor = UIColor.mainButtonBackground
            setBackgroundColor(UIColor.mainButtonBackground, for: .normal)
            
            setBackgroundColor(UIColor.mainButtonDisabledBackground, for: .disabled)
            
            setTitleColor(UIColor.mainButtonTextColor, for: .normal)
            setTitleColor(UIColor.mainButtonDisabledTextColor, for: .disabled)
            
        case .normalSecondary:
            
            backgroundColor = UIColor.secondaryButtonBackground
            setBackgroundColor(UIColor.secondaryButtonBackground, for: .normal)
            
            setBackgroundColor(UIColor.secondaryButtonDisabledBackground, for: .disabled)
            
            setTitleColor(UIColor.secondaryButtonTextColor, for: .normal)
            setTitleColor(UIColor.secondaryButtonDisabledTextColor, for: .disabled)
            
        case .popup:
            
            backgroundColor = UIColor.mainPopupButtonBackground
            setBackgroundColor(UIColor.mainPopupButtonBackground, for: .normal)
            
            setBackgroundColor(UIColor.mainPopupButtonDisabledBackground, for: .disabled)
            
            setTitleColor(UIColor.mainPopupButtonTextColor, for: .normal)
            setTitleColor(UIColor.mainPopupButtonDisabledTextColor, for: .disabled)
            
        case .popupSecondary:
            
            backgroundColor = UIColor.secondaryPopupButtonBackground
            setBackgroundColor(UIColor.secondaryPopupButtonBackground, for: .normal)
            
            setBackgroundColor(UIColor.secondaryPopupButtonDisabledBackground, for: .disabled)
            
            setTitleColor(UIColor.secondaryPopupButtonTextColor, for: .normal)
            setTitleColor(UIColor.secondaryPopupButtonDisabledTextColor, for: .disabled)
            
        }
        
        if let titleLabel = titleLabel{
            titleLabel.font = UIFont.systemFont(ofSize: TextSize.normal, weight: .semibold)
        }
        
        clipsToBounds = true
    
    }
    
    
    
    //MARK:- Public methods
    
    
}
