<<<<<<< HEAD
# iOSTABChallenge

Ramon Haro Marques TAB iOS Challenge.

When the user starts the app, the case studies provided within the json are fetched and the model gets populated. If an error occurs during the fetch process, the user will be notified and a reattemp will be offered.

The first UIViewController shows a UICollectionView with the 'hero_image' image and the 'teaser' text for each Article. An additional header has been hard coded to follow the UI guides from the demo website provided.

Upon clicking the UIViewCollectionViewCell, the user is presented with the second UIViewController that displays the full article using the requirements and some guidance from the demo website.

Both UIViewControllers are embeded on a UINavigationController and the transition is a default push transition.

The app runs with devices from iOS 10, supports rotation, uses swift 5 and has some UnitTests, as well as other features.

Some future development has been commented upon within the code. Amongst others, future development would include pop up style, extension of the base UIViewController and further UnitTesting. 
=======
# TAB_iOS

>>>>>>> 9e65825f121278c9ecd681c1ac44d7ffec469473
