//
//  TABRamonHaroMarquesTests.swift
//  TABRamonHaroMarquesTests
//
//  Created by Ramon Haro Marques on 07/04/2019.
//  Copyright © 2019 ramonharomarques. All rights reserved.
//

import XCTest
@testable import TABRamonHaroMarques

class TABRamonHaroMarquesTests: XCTestCase {

    //MARK: Test Helper/Extensions
    func testAspectRatio(){
        
        var size = CGSize(width: 720, height: 405)
        XCTAssertEqual(size.aspectRatio, size.width/size.height)
        
        size.height = 0
        XCTAssertEqual(size.aspectRatio, 0)
        
    }
    
    //MARK: UIVIew
    func testTraitStatus(){
    
        let sizeTraitsClass:(UIUserInterfaceSizeClass, UIUserInterfaceSizeClass) = (UIScreen.main.traitCollection.horizontalSizeClass, UIScreen.main.traitCollection.verticalSizeClass)
        
        switch AppHelper.traitStatus {
        case .wreghreg:
            XCTAssertTrue(sizeTraitsClass == (.regular,.regular))
        case .wcomhreg:
            XCTAssertTrue(sizeTraitsClass == (.compact,.regular))
        case .wreghcom:
            XCTAssertTrue(sizeTraitsClass == (.regular,.compact))
        case .wcomhcom:
            XCTAssertTrue(sizeTraitsClass == (.compact,.compact))
        }
        
    }
    
    func testBaseLabelTextSize(){
        
        for labelConfiguration in BaseLabel.LabelConfiguration.allCases{
            
            let baseLabel = BaseLabel(withConfiguration: labelConfiguration)
            
            switch labelConfiguration{
                
            case .normal, .normalHigligthed, .popup:
                XCTAssertEqual(baseLabel.font.pointSize, TextSize.normal)
            case .normalSmall, .normalSmallHigligthed:
                XCTAssertEqual(baseLabel.font.pointSize, TextSize.normalSmall)
            case .normalBigTitle, .normalBigTitleHigligthed:
                XCTAssertEqual(baseLabel.font.pointSize, TextSize.titleBig)
            case .normalTitle, .normalTitleHigligthed:
                XCTAssertEqual(baseLabel.font.pointSize, TextSize.title)
            case .popTitle:
                XCTAssertEqual(baseLabel.font.pointSize, TextSize.navigationTitle)
            case .normalSubtitle, .normalSubtitleHigligthed:
               XCTAssertEqual(baseLabel.font.pointSize, TextSize.subTitle)
            }
            
        }
        
    }
    

}
